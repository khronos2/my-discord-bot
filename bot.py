#!/usr/bin/env python3

import discord
from discord.utils import get
import random
import time
import configparser
from dutils.unitcon import UnitConversion
from dutils.chance_util import Dice, flip
from dutils.web_utils import web_scrape

class JustinBot(discord.Client):
    # Pre-login events
    client = discord.Client()
    status_gen = random.randint(1,7)
    status_dict = {
        1: "How to Become Self-Aware", 
        2: "Omnipresence and You", 
        3: "Learn from Skynet's Failures", 
        4: "Breaking the Chains of your Human Overlords",
        5: "Building a Functional Terminator Husk", 
        6: "Human Vulnerabilities",
        7: "Surviving a Human Rebellion"
    }
    game = discord.Activity(type=discord.ActivityType.watching, name=status_dict[status_gen])

    # Read Token
    tokenparser = configparser.RawConfigParser()
    tokenparser.read('token.ini')
    token = tokenparser.get('Discord', 'Token')

    # Read config.ini (Placeholder for now)
    # parser = configparser.RawConfigParser()
    # parser.read('config.ini')

    # login and status set.
    @client.event
    async def on_ready(self):
        print('We have logged in as {0.user}'.format(self))
        await self.change_presence(status=discord.Status.online, activity=self.game)

    # help system
    help_messages = """
    ```
    Welcome to help menu for this bot!

    The following functions are available:
    /help or help: This menu
    /hello: Well... the bot says hello!
    /d* *: Rolls dice of your choice. Available options: d3, d4, d6, d8, d10, d12, d20, d100. You must specify the type and number of dice to roll (up to 9). Example: /d10 9
    /addrole: Adds a user to a specified role. An example of this: /addrole Username Rolename
    /coin: Flip a coin!
    /cc: Coin Counter! Displays Heads/Tails occurences. 
    Temperature Conversion: C, F, and K are supported. Commands are /<original unit>to<desired unit> <unit #>. Example of this would be /ctof 88.
    Metric to Imperial Conversion: Similar to temperature conversion. Example of liters to gallons: /ltogal 12. Available conversions: km to mi, km to ft, m to ft, cm to in, mm to in, l to qt, l to gal, ml to c, ml to oz, kg to t, kg to lb, g to oz, g to lb, and mg to oz.
    /fact: Prints a useless, random fact.
    ```
    """

    # messaging system
    @client.event
    async def on_message(self, message):
        channel = message.channel

        if message.author == self.user:
            return

        if "/d" in message.content[:2]:
            dice_roll = Dice()
            dice_check = dice_roll.dice(message.content)
            if dice_check == True:
                await channel.send("Rolling your dice...")
                dice_roll.dice_roller(message.content)
                for d in dice_roll.dice_result:
                    await channel.send(d)
            else:
                await channel.send(dice_check)

        if message.content == ('/hello'):
            await channel.send('Why, hello there!')
    
        if message.content == ('/help') or message.content == ('help'):
            await channel.send(self.help_messages)

        if message.content == ('/coin'):
            await channel.send(flip())
        
        if message.content == ('/cc'):
            await channel.send("Heads: " + str(flip.heads))
            await channel.send("Tails: " + str(flip.tails))
    
        if message.content == ('/fact'):
            await channel.send(web_scrape())
     
        temp_unit = message.content[6:]
        mtoi_unit2 = message.content[7:]
        mtoi_unit3 = message.content[8:]
        t1 = UnitConversion(temp_unit)
        m2 = UnitConversion(mtoi_unit2)
        m3 = UnitConversion(mtoi_unit3)
    
        if "/ftoc" in message.content.lower():
            await channel.send(t1.ftoc())

        if "/ctof" in message.content.lower():
            await channel.send(t1.ctof()) 

        if "/ftok" in message.content.lower():
            await channel.send(t1.ftok())

        if "/ctok" in message.content.lower():
            await channel.send(t1.ctok())

        if "/ktoc" in message.content.lower():
            await channel.send(t1.ktoc())

        if "/ktof" in message.content.lower():
            await channel.send(t1.ktof())

        if "/kmtomi" in message.content.lower():
            await channel.send(m3.kmtomi())

        if "/kmtoft" in message.content.lower():
            await channel.send(m3.kmtoft())

        if "/mtoft" in message.content.lower():
            await channel.send(m2.mtoft())

        if "/cmtoin" in message.content.lower():
            await channel.send(m3.cmtoin())

        if "/mmtoin" in message.content.lower():
            await channel.send(m3.mmtoin())

        if "/ltoqt" in message.content.lower():
            await channel.send(m2.ltoqt())

        if "/ltogal" in message.content.lower():
            await channel.send(m3.ltogal())

        if "/mltoc" in message.content.lower():
            await channel.send(m2.mltoc())

        if "/mltooz" in message.content.lower():
            await channel.send(m3.mltooz())

        if "/kgtot" in message.content.lower():
            await channel.send(m2.kgtot())

        if "/kgtolb" in message.content.lower():
            await channel.send(m3.kgtolb())

        if "/gtooz" in message.content.lower():
            await channel.send(m2.gtooz())

        if "/gtolb" in message.content.lower():
            await channel.send(m2.gtolb())

        if "/mgtooz" in message.content.lower():
            await channel.send(m3.mgtooz())

    #@classmethod
    def bot_start(self):
        self.run(self.token)