from lxml import html
import requests

# Web Scraper

def web_scrape():
    page = requests.get('http://www.unkno.com/')
    tree = html.fromstring(page.content)
    fact = tree.xpath('//div[@id="content"]/text()')
    trimmed_fact = str(fact)[12:-4]
    return trimmed_fact